<?php

declare(strict_types=1);

namespace Drupal\libraries_cdn\HookHandler\Alter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for hook_library_info_alter().
 *
 * Replace Drupal libraries with cdn version.
 *
 * @see web/core/core.libraries.yml
 */
class LibraryInfoAlter implements ContainerInjectionInterface {

  /**
   * Concerned jQuery UI paths.
   */
  private const JQUERY_UI_ASSETS_PATH = 'assets/vendor/jquery.ui/';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a LibraryInfoAlter object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Alter library definition to use CDN version.
   *
   * @param array $libraries
   *   An associative array of libraries registered by $extension. Keyed by
   *   internal library name and passed by reference.
   * @param string $extension
   *   Can either be 'core' or the machine name of the extension that
   *   registered the libraries.
   */
  public function alter(array &$libraries, string $extension): void {

    if ($extension === 'core') {
      $libs = $this->entityTypeManager->getStorage('library_cdn')->loadMultiple();

      foreach ($libs as $lib) {

        $name = $lib->get('library');
        if ($lib->get('status') && isset($libraries[$name])) {

          $paths = explode("\r\n", $lib->get('paths'));
          $type = $lib->get('type');
          if ($type === 'js' && isset($libraries[$name]['js'])) {
            $data = array_values($libraries[$name]['js']);
            $weight = $data[0]['weight'];
            $this->replaceLibraryJs($libraries[$name], $paths, $weight);
          }
          elseif ($type === 'css' && isset($libraries[$name]['css']['theme'])) {
            $data = array_values($libraries[$name]['css']['theme']);
            $weight = $data[0]['weight'];
            $this->replaceLibraryCss($libraries[$name], 'theme', $paths, $weight);
          }
        }
      }

      // $this->removeLibraries($libraries, self::JQUERY_UI_ASSETS_PATH);
    }
  }

  /**
   * Replace a library js definition.
   */
  private function replaceLibraryJs(array &$library, array $paths, ?float $weight): void {
    if (!isset($library['js'])) {
      return;
    }

    $params = [
      'type' => 'external',
      'weight' => $weight ?? 1,
    ];

    $library['js'] = [];
    foreach ($paths as $path) {
      if (str_ends_with($path, '.min.js')) {
        $params['minified'] = 'true';
      }
      $library['js'][$path] = $params;
    }
  }

  /**
   * Replace a library css definition.
   */
  private function replaceLibraryCss(array &$library, string $subElem, array $paths, ?float $weight): void {
    if (!isset($library['css'][$subElem])) {
      return;
    }

    $params = [
      'type' => 'external',
      'weight' => $weight ?? 1,
    ];

    $library['css'] = [];
    foreach ($paths as $path) {
      if (str_ends_with($path, '.min.css')) {
        $params['minified'] = 'true';
      }
      $library['css'][$subElem][$path] = $params;
    }
  }

  /**
   * Remove assets references from all libraries based on a path.
   *
   * @param array $libraries
   *   An associative array of libraries registered by $extension. Keyed by
   *   internal library name and passed by reference.
   * @param string $pathIdentifier
   *   Path to find and remove.
   */
  private function removeLibraries(array &$libraries, string $pathIdentifier): void {
    foreach ($libraries as $name => $value) {
      if (!isset($libraries[$name]['js'])) {
        continue;
      }
      $assets = $libraries[$name]['js'];
      foreach ($assets as $assetPath => $assetData) {
        if (strpos($assetPath, $pathIdentifier) !== FALSE) {
          unset($libraries[$name]['js'][$assetPath]);
        }
      }

      if (!isset($libraries[$name]['css'])) {
        continue;
      }
      $assets = $libraries[$name]['css'];
      foreach (['component', 'theme'] as $cssSubElement) {
        if (!isset($assets[$cssSubElement])) {
          continue;
        }
        foreach ($assets[$cssSubElement] as $assetPath => $assetData) {
          if (strpos($assetPath, $pathIdentifier) !== FALSE) {
            unset($libraries[$name]['css'][$cssSubElement][$assetPath]);
          }
        }
      }
    }
  }

}
