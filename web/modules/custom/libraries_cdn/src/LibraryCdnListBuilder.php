<?php

namespace Drupal\libraries_cdn;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of libraries cdn.
 */
class LibraryCdnListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['library'] = $this->t('Drupal library');
    $header['type'] = $this->t('Library type');
    $header['paths'] = $this->t('CDN paths');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\libraries_cdn\LibraryCdnInterface $entity */
    $row['label'] = $entity->label();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['library'] = $entity->get('library');
    $types = [
      'js' => $this->t('JavaScript'),
      'css' => $this->t('StyleSheet'),
    ];
    $row['type'] = $types[$entity->get('type')];
    $row['paths'] = $entity->get('paths');
    return $row + parent::buildRow($entity);
  }

}
