<?php

namespace Drupal\libraries_cdn;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a library cdn entity type.
 */
interface LibraryCdnInterface extends ConfigEntityInterface {

}
