<?php

namespace Drupal\libraries_cdn\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Library CDN form.
 *
 * @property \Drupal\libraries_cdn\LibraryCdnInterface $entity
 */
class LibraryCdnForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the library cdn.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\libraries_cdn\Entity\LibraryCdn::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['library'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 255,
      '#title' => $this->t('Drupal library'),
      '#default_value' => $this->entity->get('library'),
      '#description' => $this->t('The Drupal core library name to replace with CDN version.'),
      '#required' => TRUE,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Library type'),
      '#options' => [
        'js' => $this->t('JavaScript'),
        'css' => $this->t('StyleSheet'),
      ],
      '#default_value' => $this->entity->get('type'),
      '#description' => $this->t('Select the type of the library.'),
    ];

    $form['paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CDN paths'),
      '#default_value' => $this->entity->get('paths'),
      '#description' => $this->t('CDN path of the library, one per line.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new library cdn %label.', $message_args)
      : $this->t('Updated library cdn %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
