<?php

namespace Drupal\libraries_cdn\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\libraries_cdn\LibraryCdnInterface;

/**
 * Defines the library cdn entity type.
 *
 * @ConfigEntityType(
 *   id = "library_cdn",
 *   label = @Translation("Library CDN"),
 *   label_collection = @Translation("Libraries CDN"),
 *   label_singular = @Translation("library cdn"),
 *   label_plural = @Translation("libraries cdn"),
 *   label_count = @PluralTranslation(
 *     singular = "@count library cdn",
 *     plural = "@count libraries cdn",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\libraries_cdn\LibraryCdnListBuilder",
 *     "form" = {
 *       "add" = "Drupal\libraries_cdn\Form\LibraryCdnForm",
 *       "edit" = "Drupal\libraries_cdn\Form\LibraryCdnForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "library_cdn",
 *   admin_permission = "administer library_cdn",
 *   links = {
 *     "collection" = "/admin/structure/library-cdn",
 *     "add-form" = "/admin/structure/library-cdn/add",
 *     "edit-form" = "/admin/structure/library-cdn/{library_cdn}",
 *     "delete-form" = "/admin/structure/library-cdn/{library_cdn}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "library",
 *     "paths",
 *     "type"
 *   }
 * )
 */
class LibraryCdn extends ConfigEntityBase implements LibraryCdnInterface {

  /**
   * The library cdn ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The library cdn label.
   *
   * @var string
   */
  protected $label;

  /**
   * The library cdn status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The library name to swap.
   *
   * @var string
   */
  protected $library;

  /**
   * The library CDN paths.
   *
   * @var string
   */
  protected $paths;

  /**
   * The library type.
   *
   * @var string
   */
  protected $type;

}
