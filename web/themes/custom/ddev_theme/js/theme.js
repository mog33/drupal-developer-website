/**
 * @file
 * my_sub_theme behaviors.
 */

(function ($, Drupal) {
  "use strict";

  /**
   * Behavior description.
   */
  Drupal.behaviors.oliveroSubTheme = {
    attach: function (context, settings) {
      /**
       * Back to top
       */
      let backToTopButton = document.getElementById("btn-back-to-top");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function () {
        scrollBackToTopFunction();
      };

      function scrollBackToTopFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          backToTopButton.style.display = "block";
        } else {
          backToTopButton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      backToTopButton.addEventListener("click", backToTop);

      function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    },
  };
})(jQuery, Drupal);
